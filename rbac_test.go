package rbac

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestManager_Load(t *testing.T) {
	testScope := "test_scope"
	testRoleName := "test_role_name"
	testRoleName2 := "test_role_name_2"
	testPermissionName1 := "permission_1"
	testPermissionAllowed1 := true
	testPermissionName2 := "permission_2"
	testPermissionAllowed2 := false
	testPermissionName3 := "permission_1"
	testPermissionAllowed3 := false

	tests := map[string]struct {
		mockStore   func(t *testing.T) (*MockStore, func())
		assertError func(t *testing.T, err error)
		assertRoles func(t *testing.T, manager *Manager)
	}{
		"load failure": {
			mockStore: func(t *testing.T) (*MockStore, func()) {
				store := new(MockStore)
				store.On("Load", testScope).
					Return(nil, assert.AnError).
					Once()

				return store, func() {
					store.AssertExpectations(t)
				}
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorIs(t, err, assert.AnError)
			},
		},
		"no roles for scope": {
			mockStore: func(t *testing.T) (*MockStore, func()) {
				store := new(MockStore)
				store.On("Load", testScope).
					Return(make([]Role, 0), nil).
					Once()

				return store, func() {
					store.AssertExpectations(t)
				}
			},
			assertError: nil,
			assertRoles: func(t *testing.T, manager *Manager) {
				assert.Empty(t, manager.checker.roles)
			},
		},
		"duplicated role names": {
			mockStore: func(t *testing.T) (*MockStore, func()) {
				role1 := new(MockRole)
				role1.On("Name").Return(testRoleName).Once()
				role1.On("Permissions").Return(nil).Once()

				role2 := new(MockRole)
				role2.On("Name").Return(testRoleName).Once()
				role2.On("Permissions").Return(nil).Once()

				store := new(MockStore)
				store.On("Load", testScope).
					Return([]Role{role1, role2}, nil).
					Once()

				return store, func() {
					store.AssertExpectations(t)

					role1.AssertExpectations(t)
					role2.AssertExpectations(t)
				}
			},
			assertError: func(t *testing.T, err error) {
				var e *RoleAlreadyExistsError
				if assert.ErrorAs(t, err, &e) {
					assert.Equal(t, testScope, e.scope)
					assert.Equal(t, testRoleName, e.name)
				}
			},
		},
		"roles found": {
			mockStore: func(t *testing.T) (*MockStore, func()) {
				role1 := new(MockRole)
				role1.On("Name").Return(testRoleName).Once()
				role1.On("Permissions").Return(map[string]bool{
					testPermissionName1: testPermissionAllowed1,
					testPermissionName2: testPermissionAllowed2,
				}).Once()

				role2 := new(MockRole)
				role2.On("Name").Return(testRoleName2).Once()
				role2.On("Permissions").Return(map[string]bool{
					testPermissionName2: testPermissionAllowed2,
					testPermissionName3: testPermissionAllowed3,
				}).Once()

				store := new(MockStore)
				store.On("Load", testScope).
					Return([]Role{role1, role2}, nil).
					Once()

				return store, func() {
					store.AssertExpectations(t)
					role1.AssertExpectations(t)
					role2.AssertExpectations(t)
				}
			},
			assertError: nil,
			assertRoles: func(t *testing.T, manager *Manager) {
				checker := manager.checker

				assert.NotEmpty(t, checker.roles)

				require.Contains(t, checker.roles, testRoleName)
				require.Contains(t, checker.roles[testRoleName], testPermissionName1)
				assert.Equal(t, testPermissionAllowed1, checker.roles[testRoleName][testPermissionName1])
				require.Contains(t, checker.roles[testRoleName], testPermissionName2)
				assert.Equal(t, testPermissionAllowed2, checker.roles[testRoleName][testPermissionName2])

				require.Contains(t, checker.roles, testRoleName2)
				require.Contains(t, checker.roles[testRoleName2], testPermissionName1)
				assert.Equal(t, testPermissionAllowed2, checker.roles[testRoleName2][testPermissionName2])
				require.Contains(t, checker.roles[testRoleName2], testPermissionName3)
				assert.Equal(t, testPermissionAllowed3, checker.roles[testRoleName2][testPermissionName3])
			},
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			require.NotNil(t, tt.mockStore)

			store, storeAssertions := tt.mockStore(t)
			defer storeAssertions()

			manager := New(store)
			err := manager.Load(testScope)

			if tt.assertError != nil {
				tt.assertError(t, err)
				return
			}

			assert.NoError(t, err)
			require.NotNil(t, tt.assertRoles)

			tt.assertRoles(t, manager)
		})
	}
}

func TestManager_IsPermitted(t *testing.T) {
	testScope := "test_scope"

	testRole1 := "test_role_1"
	testRole2 := "test_role_2"
	testRole3 := "test_role_3"
	testRole4 := "test_role_4"

	testPermissionName1 := "test_permission_name_1"
	testPermissionAllowed1 := false
	testPermissionName2 := "test_permission_name_2"
	testPermissionAllowed2 := true
	testPermissionName3 := "test_permission_name_1"
	testPermissionAllowed3 := true

	tests := map[string]struct {
		mockActor      func(t *testing.T) (*MockActor, func())
		expectedResult bool
	}{
		"no roles assigned to the actor": {
			mockActor: func(t *testing.T) (*MockActor, func()) {
				actor := new(MockActor)
				actor.On("RoleNames", testScope).Return(nil).Once()

				return actor, func() {
					actor.AssertExpectations(t)
				}
			},
			expectedResult: false,
		},
		"no roles matching the repository": {
			mockActor: func(t *testing.T) (*MockActor, func()) {
				actor := new(MockActor)
				actor.On("RoleNames", testScope).
					Return([]string{
						testRole4,
					}).
					Once()

				return actor, func() {
					actor.AssertExpectations(t)
				}
			},
			expectedResult: false,
		},
		"one role defining specific permission": {
			mockActor: func(t *testing.T) (*MockActor, func()) {
				actor := new(MockActor)
				actor.On("RoleNames", testScope).
					Return([]string{
						testRole1,
					}).
					Once()

				return actor, func() {
					actor.AssertExpectations(t)
				}
			},
			expectedResult: false,
		},
		"permission not defined in the roles": {
			mockActor: func(t *testing.T) (*MockActor, func()) {
				actor := new(MockActor)
				actor.On("RoleNames", testScope).
					Return([]string{
						testRole3,
					}).
					Once()

				return actor, func() {
					actor.AssertExpectations(t)
				}
			},
			expectedResult: false,
		},
		"two roles differently defining the same permission": {
			mockActor: func(t *testing.T) (*MockActor, func()) {
				actor := new(MockActor)
				actor.On("RoleNames", testScope).
					Return([]string{
						testRole1,
						testRole2,
					}).
					Once()

				return actor, func() {
					actor.AssertExpectations(t)
				}
			},
			expectedResult: true,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			actor, actorAssertions := tt.mockActor(t)
			defer actorAssertions()

			checker := newChecker()
			checker.scope = testScope
			checker.roles = rolePermissionsMap{
				testRole1: {
					testPermissionName1: testPermissionAllowed1,
					testPermissionName2: testPermissionAllowed2,
				},
				testRole2: {
					testPermissionName2: testPermissionAllowed2,
					testPermissionName3: testPermissionAllowed3,
				},
				testRole3: {
					testPermissionName2: testPermissionAllowed2,
				},
			}

			manager := &Manager{
				checker: checker,
			}
			result := manager.IsPermitted(actor, testPermissionName1)

			assert.Equal(t, tt.expectedResult, result)
		})
	}
}
