package rbac

import (
	"fmt"
)

type RoleAlreadyExistsError struct {
	scope string
	name  string
}

func NewRoleAlreadyExistsError(scope string, name string) *RoleAlreadyExistsError {
	return &RoleAlreadyExistsError{
		scope: scope,
		name:  name,
	}
}

func (e *RoleAlreadyExistsError) Error() string {
	return fmt.Sprintf("role '%s:%s' is already defined", e.scope, e.name)
}

type rolePermissionsMap map[string]permissionsMap

type permissionsMap map[string]bool

type checker struct {
	scope string
	roles rolePermissionsMap
}

func newChecker() *checker {
	return &checker{
		roles: make(rolePermissionsMap),
	}
}

func (c *checker) addRolePermissions(name string, permissions permissionsMap) error {
	_, ok := c.roles[name]
	if ok {
		return NewRoleAlreadyExistsError(c.scope, name)
	}

	c.roles[name] = make(permissionsMap, len(permissions))
	for permission, allowed := range permissions {
		c.roles[name][permission] = allowed
	}

	return nil
}

func (c *checker) isPermitted(actor Actor, permission string) bool {
	roles := actor.RoleNames(c.scope)
	if len(roles) < 1 {
		return false
	}

	allowed := false
	for _, roleName := range roles {
		permissions, ok := c.roles[roleName]
		if !ok {
			continue
		}

		a, ok := permissions[permission]
		if !ok {
			continue
		}

		allowed = allowed || a
	}

	return allowed
}

type Manager struct {
	store   Store
	checker *checker
}

func New(store Store) *Manager {
	return &Manager{
		store:   store,
		checker: newChecker(),
	}
}

func (m *Manager) Load(scope string) error {
	roles, err := m.store.Load(scope)
	if err != nil {
		return fmt.Errorf("loading roles for %q: %w", scope, err)
	}

	m.checker.scope = scope

	for _, role := range roles {
		err = m.checker.addRolePermissions(role.Name(), role.Permissions())
		if err != nil {
			return err
		}
	}

	return nil
}

func (m *Manager) IsPermitted(actor Actor, permission string) bool {
	return m.checker.isPermitted(actor, permission)
}

type Actor interface {
	RoleNames(scope string) []string
}

type Store interface {
	Load(scope string) ([]Role, error)
}

type Role interface {
	Name() string
	Permissions() map[string]bool
}
