MOCKERY_VERSION ?= 2.14.0
MOCKERY ?= .tmp/mockery-$(MOCKERY_VERSION)

.PHONY: mocks
mocks: $(MOCKERY)
	# Deleting existing mock files
	@find . -type f -name 'mock_*' -delete
	# Generating new mock files
	$(MOCKERY) --dir . --all --inpackage --testonly

.PHONY: check_mocks
check_mocks:
	# Checking if mocks are up-to-date
	@$(MAKE) mocks
	# Checking the differences
	@git --no-pager diff --compact-summary --exit-code -- $(shell git ls-files | grep 'mock_') && \
                !(git ls-files -o | grep 'mock_') && \
                echo "Mocks up-to-date!"

$(MOCKERY): OS_TYPE ?= $(shell uname -s)
$(MOCKERY): DOWNLOAD_URL = "https://github.com/vektra/mockery/releases/download/v$(MOCKERY_VERSION)/mockery_$(MOCKERY_VERSION)_$(OS_TYPE)_x86_64.tar.gz"
$(MOCKERY):
	# Installing $(DOWNLOAD_URL) as $(MOCKERY)
	@mkdir -p $(shell dirname $(MOCKERY))
	@curl -sL "$(DOWNLOAD_URL)" | tar xz -O mockery > $(MOCKERY)
	@chmod +x "$(MOCKERY)"

